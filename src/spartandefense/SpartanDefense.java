/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package spartandefense;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.*;
import javax.swing.*;
import javax.swing.border.BevelBorder;

/**
 * TO-DO: ADD DIRECTIONS (ie, char is facing left/right/up/down) - this will allow for AoE combat (semi-circle in direction is hit box - all enemies in it recieve damage).
 * @author Moody
 */
public class SpartanDefense {
    static char[][] mapArray; // only eused in this class to pass to MapKeyListener... but mapKeyListener calls SpartanDefense.mapArray every moveColumn() or moveRow()
    static File mapFile;
    static MapGUI mapGUI;
    static BufferedReader mapReader;
    static int tX=0,tY=0, enemiesRemaining = 0, pl_score=0;
    static int[] tempPlPos = new int[2];
    static Player pl;
    static Enemy[] enemyList;

    public static void main(String[] args) throws IOException, FileNotFoundException {
        chooseMap();
        size();
        readMap(tX, tY);
        initEnemies();
        mapGUI = new MapGUI(mapArray, tX, tY);
    }
    
    public static void chooseMap(){
        JFileChooser mapChooser = new JFileChooser();
        int choice = mapChooser.showOpenDialog(null);
        mapFile = mapChooser.getSelectedFile();
        if(choice == JFileChooser.APPROVE_OPTION)
            JOptionPane.showMessageDialog(null, mapFile + " opened successfully.");
    }
        
    public static void readMap(int rows, int cols) throws FileNotFoundException, IOException{
        String line="";
        mapArray = new char[rows][cols];
        mapReader = new BufferedReader(new FileReader(mapFile));
        int k = 0;
        
        //while loop reads line by line, char by char
        while(true) {
            line = mapReader.readLine();
            if(line == null)
                break;
            //Char reader
            for(int x=0; x<line.length();x++){
                if(line.charAt(x) != '\n'){
                    mapArray[x][k] = line.charAt(x);
                    //When it runs across any character, it creates the player
                    if(line.charAt(x) == Map.PL_CHAR_UP || line.charAt(x) == Map.PL_CHAR_DOWN
                            || line.charAt(x) == Map.PL_CHAR_LEFT || line.charAt(x) == Map.PL_CHAR_RIGHT){
                        tempPlPos[0] = x;
                        tempPlPos[1] = k;
                        pl = new Player(tempPlPos, line.charAt(x));
                    }
                }
                if(line.charAt(x) == Map.NPC_CHAR){
                    mapArray[x][k] = line.charAt(x);
                    enemiesRemaining++;
                }
            }
            k++;
        } //endWhile
    }
    
    public static void initEnemies() {
        enemyList = new Enemy[enemiesRemaining];
        int counter = 0;
        for(int y=0; y<tY; y++) {
            for(int x=0; x<tX; x++){
                if(mapArray[x][y] == Map.NPC_CHAR) {
                    enemyList[counter] = new Enemy(counter, x, y);
                    counter++;
                    //if counter == enemies remaining (+/- 1?) break;//dont search whole map - only search until you've found all the enemies
                }
            }
        }
    }
    
    public static void size() throws FileNotFoundException, IOException{
        mapReader = new BufferedReader(new FileReader(mapFile));
        String line;
        while(true){
            line = mapReader.readLine();
            if(line == null)
                break;
            tX = line.length(); //should be same length every line.
            tY++;
        }
        mapReader.close();
    }
    
}
