/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package spartandefense;

import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.*;
import javax.swing.border.BevelBorder;

/**
 *
 * @author Moody
 */
public class MapGUI extends JFrame{
    MapWindow mapWin;
    
    public MapGUI(char[][] mapArray, int tX, int tY) {
        super("Spartan Defense");
        
        
        mapWin = new MapWindow(mapArray, tX, tY);
        mapWin.setSize(tX*15, tX*25);
        add(mapWin);
        addKeyListener(MapKeyListener.keyL);
        
        JPanel statusPanel = new JPanel();
        statusPanel.setBorder(new BevelBorder(BevelBorder.LOWERED));
        add(statusPanel, BorderLayout.SOUTH);
        statusPanel.setPreferredSize(new Dimension(getWidth(), 16));
        statusPanel.setLayout(new BoxLayout(statusPanel, BoxLayout.X_AXIS));
        JLabel statusLabel = new JLabel("HP: "+SpartanDefense.pl.getHP());
        statusLabel.setHorizontalAlignment(SwingConstants.LEFT);
        statusPanel.add(statusLabel);
        
        setSize(mapWin.getWidth(), mapWin.getHeight()+16); //16 is height of status bar
        setVisible(true);
    }
    
    public MapWindow getMapWin() {return mapWin;}
    
    
}
