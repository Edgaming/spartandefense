/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package spartandefense;

/**
 *
 * @author Moody
 */
public class Attack {
    private String name;
    private int attType, dmg, staminaCost, range;
    
    public final static int POINT_ATTK=1;
    public final static int CONE_ATTK=2;
    public final static int AOE_ATTK=3;
    
    public Attack(String name, int dmg, int staminaCost, int attType) {
        this.name = name;
        this.dmg = dmg;
        this.staminaCost = staminaCost;
        this.attType = attType;
        range = 3;
    }
    
    public String getName() {return name;}
    public int getDmg() {return dmg;}
    public int getStamCost() {return staminaCost;}
    public int getType() {return attType;}
    public int getRange() {return range;}
 
    public void setName(String name){this.name = name;}
    public void setDmg(int dmg) {this.dmg = dmg;}
    public void setStamCost(int staminaCost) {this.staminaCost = staminaCost;}
    public void setRange(int range) {this.range = range;}
}
