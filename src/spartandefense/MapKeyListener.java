/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package spartandefense;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JOptionPane;

/**
 *
 * @author Moody
 */
public class MapKeyListener extends java.awt.event.KeyAdapter {

    static public char[][] mapArray = SpartanDefense.mapArray;
    static boolean isShielded = false;
    public static final KeyListener keyL = new KeyListener() {
        @Override
        public void keyTyped(KeyEvent e) {
            char keyPress = e.getKeyChar();
            switch (keyPress) {
                case 'w':
                case 'W':
                    if(SpartanDefense.pl.getDirection() == Map.PL_CHAR_UP)
                        moveColumn(Map.UP);
                    else 
                        changeDir(Map.PL_CHAR_UP);
                    break;
                case 's':
                case 'S':
                    if(SpartanDefense.pl.getDirection() == Map.PL_CHAR_DOWN)
                        moveColumn(Map.DOWN);
                    else
                        changeDir(Map.PL_CHAR_DOWN);
                    break;
                case 'a':
                case 'A':
                    if(SpartanDefense.pl.getDirection() == Map.PL_CHAR_LEFT)
                        moveRow(Map.LEFT);
                    else
                        changeDir(Map.PL_CHAR_LEFT);
                    break;
                case 'd':
                case 'D':
                    if(SpartanDefense.pl.getDirection() == Map.PL_CHAR_RIGHT)
                        moveRow(Map.RIGHT);
                    else
                        changeDir(Map.PL_CHAR_RIGHT);
                    break;
                    
                case 'q':
                case 'Q':
                    //ATTACK1
                    initAttack(0);//Q=0
                    break;
                
                case 'e':
                case 'E':
                    //ATTACK2
                    initAttack(1); //E=1
                default:
                    break; //Do Nothing for other characters
            }
        }

        public void keyPressed(KeyEvent e) {
            char keyPress = e.getKeyChar();
            switch (keyPress) {
                case 'r':
                case 'R':
                    //shield
                    //remember direction
                    if(!isShielded){
                        SpartanDefense.mapGUI.mapWin.toggleShield(isShielded);
                        isShielded = true;
                        SpartanDefense.pl.setCurrentChar(Map.PL_CHAR_SHIELD);
                    }
                    break;
            }
        }

        public void keyReleased(KeyEvent e) {
            //back to last known direction
            char keyPress = e.getKeyChar();
            switch (keyPress) {
                case 'r':case'R':
                    SpartanDefense.mapGUI.mapWin.toggleShield(isShielded);
                    isShielded = false;
                    SpartanDefense.pl.setCurrentChar(SpartanDefense.pl.getDirection());
                    break;
            }
        }
    };

    public static void changeDir(char c){
        SpartanDefense.pl.setDirection(c);
        char toShow;
        if(!isShielded){    
            SpartanDefense.pl.setCurrentChar(c);
            toShow = c;
        } else {
            SpartanDefense.pl.setCurrentChar(Map.PL_CHAR_SHIELD);
            toShow = Map.PL_CHAR_SHIELD;
        }
        mapArray[SpartanDefense.pl.getXCoord()][SpartanDefense.pl.getYCoord()] = toShow;
        SpartanDefense.mapGUI.getMapWin().changePlayerDir(toShow);
    }

    public static void moveColumn(int dir){
        int x = SpartanDefense.pl.getXCoord();
        int y = SpartanDefense.pl.getYCoord();
        int arrivalY = y + dir;
        
        int[] start = {x,y}, tar = {x,arrivalY};
        
        if(mapArray[x][arrivalY] == ' ') { //OR IF ITS EQUAL TO BOOST
            mapArray[x][arrivalY] = mapArray[x][y];
            mapArray[x][y] = ' ';
            SpartanDefense.pl.setYCoord(arrivalY);
            SpartanDefense.mapGUI.getMapWin().movePlayer(tar, start);
        } else { //CANT MOVE INTO ENEMIES, MUST DESTROY THEM
            //do nothing bitch
        }
    }
    
    public static void moveRow(int dir) {
        int x = SpartanDefense.pl.getXCoord();
        int y = SpartanDefense.pl.getYCoord();
        int arrivalX = x + dir;
        
        int[] start = {x,y}, tar = {arrivalX,y};
        
        if(mapArray[arrivalX][y] == ' ') { //OR IF ITS EQUAL TO BOOST
            mapArray[arrivalX][y] = mapArray[x][y];
            mapArray[x][y] = ' ';
            SpartanDefense.pl.setXCoord(arrivalX);
            SpartanDefense.mapGUI.getMapWin().movePlayer(tar, start);
        } else { //CANT MOVE INTO ENEMIES, MUST DESTROY THEM
            //do nothing bitch
        }
    }
    
    public static void initAttack(int attackNum) {
        int plX = SpartanDefense.pl.getXCoord();
        int plY = SpartanDefense.pl.getYCoord();
        Attack attack = SpartanDefense.pl.getAttacks()[attackNum];
        int attType = attack.getType();
        
        if(attType == Attack.POINT_ATTK) {
            analyzePoint(attack);
        } else if(attType == Attack.CONE_ATTK) {
            analyzeCone(attack);
        } else if(attType == Attack.AOE_ATTK) {
            analyzeAoE(attack);
        }
    }
    
    public static void analyzePoint(Attack att){
        char dir = SpartanDefense.pl.getDirection();
        int plX = SpartanDefense.pl.getXCoord();
        int plY = SpartanDefense.pl.getYCoord();
        int dirCalc = 0;
        if(dir == Map.PL_CHAR_UP || dir == Map.PL_CHAR_DOWN){
            if(dir == Map.PL_CHAR_UP)
                dirCalc = Map.UP;
            else if(dir == Map.PL_CHAR_DOWN)
                dirCalc = Map.DOWN;
            for(int y=1; y <= att.getRange(); y++){
                try {
                    if(mapArray[plX][(dirCalc*y)+plY] == Map.NPC_CHAR) {
                        damageEnemy(att, plX,((dirCalc*y)+plY));
                        break; //ONLY HIT FIRST ENEMY IN SIGHT, KILL LOOP ONCE HIT
                    }
                } catch(ArrayIndexOutOfBoundsException a) {
                    JOptionPane.showMessageDialog(null, "CAUGHT ARRAY SNOOPING OUT OF BOUNDS: "+a.getMessage());
                }
            }
        } else if(dir == Map.PL_CHAR_RIGHT || dir == Map.PL_CHAR_LEFT) {
            if(dir == Map.PL_CHAR_RIGHT)
                dirCalc = Map.RIGHT;
            else if(dir == Map.PL_CHAR_LEFT)
                dirCalc = Map.LEFT;
            for(int x=1; x<=att.getRange(); x++) {
                try {
                    if(mapArray[plX+(dirCalc*x)][plY] == Map.NPC_CHAR) {
                        damageEnemy(att,(plX+(dirCalc*x)), plY);
                        break;
                    }
                } catch(ArrayIndexOutOfBoundsException a) {
                    JOptionPane.showMessageDialog(null, "CAUGHT ARRAY SNOOPING OUT OF BOUNDS: "+a.getMessage());
                }
            }
        }
    }
    
    public static void analyzeCone(Attack att) {
        char dir = SpartanDefense.pl.getDirection();
        int plX = SpartanDefense.pl.getXCoord();
        int plY = SpartanDefense.pl.getYCoord();
        int dirCalc = 0;
        
        if(dir == Map.PL_CHAR_UP || dir == Map.PL_CHAR_DOWN){
            if(dir == Map.PL_CHAR_UP) 
                dirCalc = Map.UP;
            else if(dir==Map.PL_CHAR_DOWN) 
                dirCalc = Map.DOWN;
            
            for(int y=1; y<=att.getRange(); y++) {
                for(int x=-1; x<2; x++) {
                    try{
                        if(mapArray[plX+x][plY+(y*dirCalc)] == Map.NPC_CHAR) {
                            damageEnemy(att, plX+x, (plY+(y*dirCalc)));
                        }
                    } catch(ArrayIndexOutOfBoundsException a) {
                        JOptionPane.showMessageDialog(null, "CAUGHT ARRAY SNOOPING OUT OF BOUNDS: "+a.getMessage());
                    }
                }
            }
        } else if(dir == Map.PL_CHAR_RIGHT || dir == Map.PL_CHAR_LEFT) {
            if(dir == Map.PL_CHAR_RIGHT)
                dirCalc = Map.RIGHT;
            else if(dir == Map.PL_CHAR_LEFT)
                dirCalc = Map.LEFT;
            
            for(int x=1; x<= att.getRange(); x++) {
                for(int y=-1; y<2; y++) {
                    try{
                        if(mapArray[plX + (x*dirCalc)][plY + y] == Map.NPC_CHAR)
                            damageEnemy(att, plX+(x*dirCalc), plY+y);
                    } catch(ArrayIndexOutOfBoundsException a) {
                        JOptionPane.showMessageDialog(null, "CAUGHT ARRAY SNOOPING OUT OF BOUNDS: "+a.getMessage());
                    }
                }
            }
        }
        
    }
           
    public static void analyzeAoE(Attack att) {
        int plX = SpartanDefense.pl.getXCoord();
        int plY = SpartanDefense.pl.getYCoord();
        
        for(int x=-1; x<2; x++) {
            for(int y=-1; y<2; y++) {
                try{
                    if(mapArray[x+plX][y+plY] == Map.NPC_CHAR)
                        damageEnemy(att, x+plX, y+plY);
                } catch(ArrayIndexOutOfBoundsException a) {
                        JOptionPane.showMessageDialog(null, "CAUGHT ARRAY SNOOPING OUT OF BOUNDS: "+a.getMessage());
                }
                
            }
        }
    }
    
    public static void damageEnemy(Attack a, int x, int y) {
        Enemy en = getEnemyAt(x,y);
        int ID = en.getID(); //refers to array index. starts at 0
        int health = en.getHP() - a.getDmg();
        if(health>0) {
            SpartanDefense.enemyList[ID].setHP(health);
            System.out.println("Hit enemy with "+a.getName()+" for "+a.getDmg()+" dmg.\nEnemy has "+health+" remaining.\n\n");
        }
        else { //UNDRAW
            
            mapArray[x][y] = ' ';
            SpartanDefense.enemiesRemaining--;
            SpartanDefense.mapGUI.getMapWin().removeLabel(x, y);
            System.out.println("ENEMY-"+ID+" KILLED");
            //SpartanDefense.initEnemies(); //NOT WORKING?! - WHAT DOES THIS MEAN, to find enemies, we search for '.', if that is not there, then technically there is no enemy? 
            //when we reach 0,then bring in a new wave and reinit enemies?
        }
    }
    
    public static Enemy getEnemyAt(int x, int y) {
        Enemy en = new Enemy();
        for(int a=0; a<SpartanDefense.enemyList.length; a++) {
            if(SpartanDefense.enemyList[a].getX() == x && SpartanDefense.enemyList[a].getY() == y){
                en = SpartanDefense.enemyList[a];
            }
        }
       return en;
    }
}
