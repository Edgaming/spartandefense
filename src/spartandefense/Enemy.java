/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package spartandefense;

/**
 *
 * @author Moody
 */
public class Enemy {
    int ID, HP, stamina, maxStam, x, y;
    Attack atk;
    boolean isDead = false;
    
    public Enemy() {
        
    }
    public Enemy(int ID, int x, int y){
        this.ID = ID;
        this.x = x;
        this.y = y;
        HP=150; //no max HP cause enemy HP won't regen, only their stamina will
        stamina=100;
        maxStam=100;
        atk = new Attack("Stab", 15, 25, Attack.POINT_ATTK);
    }
    
    public int getHP() {return HP;}
    public int getStamina() {return stamina;}
    public int getMaxStamina() {return maxStam;}
    public Attack getAttack() {return atk;}
    public int getX() {return x;}
    public int getY() {return y;}
    public int getID() {return ID;}
    
    public void setHP(int HP) {
            this.HP = HP;
    }
    public void setStam(int stamina) {this.stamina = stamina;}
    public void setMaxStamina(int maxStam) {this.maxStam = maxStam;}
    public void setAttack(Attack atk) {this.atk = atk;}
    public void setX(int x) {this.x = x;}
    public void setY(int y) {this.y = y;}
}
