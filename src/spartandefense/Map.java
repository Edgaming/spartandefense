/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package spartandefense;

/**
 *
 * @author Moody
 */
public class Map {

    public char[][] mapArray;
    public Map(char[][] arr, int rows, int cols) {
        mapArray = new char[rows][cols];
        mapArray = arr;
    }
   
    /*
     ******** Constants for the map symbols.*********
     */
    //visual representation of an unpassable Wall.
    public static final char WALL = '#';
    
    //visual representation of an unpassable Hall.
    public static final char HALL = '|';
    
    //visual representation of an unpassable object.
    public static final char OBJ = '_';
    
    //visual representation of a Wall.
    public static final char HP_BOOST = '@';
    
    //visual representation of an Attack Boost.
    public static final char ATK_BOOST = '%';
    
    //visual representation of a character facing upwards.
    public static final char PL_CHAR_UP = '^';    
    //visual rep of a char facing downwards
    public static final char PL_CHAR_DOWN = 'v';
    //visual rep of a char facing downwards
    public static final char PL_CHAR_LEFT = '<';
    //visual rep of a char facing downwards
    public static final char PL_CHAR_RIGHT = '>';
    public static final char PL_CHAR_SHIELD = 'X';
    
    //visual representation of a Wall.
    public static final char NPC_CHAR = '.';
    
    /*
     *****Constants for the directions.****
     * Used mathematically to calculate positioning.
     */
    
    public static final int LEFT = -1;
    
    public static final int RIGHT = 1;
   
    public static final int NO_CHANGE = 0;
  
    public static final int UP = -1;
 
    public static final int DOWN = 1;
    
    /*
     * 
     */
}
