/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package spartandefense;

import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Constructs the actual Map gui and representation of the ASCII map.
 *
 * @author Moody
 */
public class MapWindow extends JPanel {

    public JLabel[][] mapLabels = new JLabel[SpartanDefense.tX][SpartanDefense.tY];
    public char[][] mapArray;

    /**
     * 
     * @param mapArray
     * @param x
     * @param y 
     */
    public MapWindow(char[][] mapArray, int x, int y) {
        this.mapArray = mapArray;
        setJLabels(mapArray);
        setLayout(new GridLayout(y, x));

        for (int i = 0; i < y; i++) {
            for (int n = 0; n < x; n++) {
                add(mapLabels[n][i]);
            }
        }
    }

    /**
     * Sets the JLabel's for the MapWindow class.
     *
     * @param mapLabs Assigns mapLabels to this parameter.
     */
    public void setJLabels(char[][] mapLabs) {
        for (int i = 0; i < SpartanDefense.tY; i++) {
            for (int n = 0; n < SpartanDefense.tX; n++) {
                mapLabels[n][i] = new JLabel(String.valueOf(mapLabs[n][i]));
            }
        }
    }

    /**
     * This method, switches the two specified JLabel's with each other,
     * emulating the movement of the char. -- USELESS, old assignment used to switch dots, but its just hallways.
     *
     * @param start x,y coordinates of start
     * @param tar x,y coordinates of target position
     
    public void changeTwoLabels(int[] start, int[] tar) {
        int[] temp = new int[2];
        temp = start; //placeholder value for Start
        start = tar; // change Start to target
        mapLabels[start[0]][start[1]].setText(String.valueOf(mapArray[tar[0]][tar[1]]));
        tar = temp; //change Target to value in temp (which is initial Start value)
        mapLabels[tar[0]][tar[1]].setText(String.valueOf(mapArray[temp[0]][temp[1]]));
    }*/
    
    public void movePlayer(int[] dest, int[] start) {
        mapLabels[dest[0]][dest[1]].setText(String.valueOf(SpartanDefense.pl.getCurrentChar()));
        mapLabels[start[0]][start[1]].setText(" ");
    }
    
    /**
     * Checks if character is shielded, if he is, it unshields him. If he isn't, it shields him.
     * @param isShielded variable to check if char is shielded.
     */
    public void toggleShield(boolean isShielded){
        int x = SpartanDefense.pl.getXCoord();
        int y = SpartanDefense.pl.getYCoord();
        
        
        if(isShielded) {
            mapLabels[x][y].setText(String.valueOf(SpartanDefense.pl.getDirection())); //CHANGE TO LAST KNOWN DIRECTION
        } else{
            mapLabels[x][y].setText(String.valueOf(Map.PL_CHAR_SHIELD));
        }
        
    }
    
    public void changePlayerDir(char c) {
        mapLabels[SpartanDefense.pl.getXCoord()][SpartanDefense.pl.getYCoord()].setText(String.valueOf(c));
    }
    
    public void removeLabel(int x, int y) {
        mapLabels[x][y].setText(" ");
    }
}
