/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package spartandefense;

/**
 *
 * @author Moody
 */
public class Player {
    private int currHP=100, maxHP=100, currStamina=200, maxStamina=200, shieldDura=150; //switch stamina to rage - buildup rage with Q, release with E attack
    private Attack[] atkList = new Attack[2];
    private int[] pos = new int[2]; //X-Y coordinates of player. [0]=X, [1]=Y
    private char currentChar; //to help moving a shielded player
    private char plDir; //to help when unshielding. can change while player is shielded
    
    public Player(int[] pos, char c) {
        atkList[0] = new Attack("Power Swing", 100, 25, Attack.AOE_ATTK);
        atkList[1] = new Attack("Shout", 75, 100, Attack.CONE_ATTK);
        this.pos = pos;
        currentChar = c;
        plDir = c;
    }
    
    public int getHP() {return currHP;}
    public int getMaxHP() {return maxHP;}
    public int getStam() {return currStamina;}
    public int getMaxStam() {return maxStamina;}
    public Attack[] getAttacks() {return atkList;}
    public int[] getPos() {return pos;}
    public int getXCoord() {return pos[0];}
    public int getYCoord() {return pos[1];}
    public char getCurrentChar() {return currentChar;}
    public char getDirection() {return plDir;}
    public int getShieldDura() {return shieldDura;}
    
    public void setHP(int currHP) {this.currHP = currHP;}
    public void setMaxHP(int maxHP) {this.maxHP = maxHP;}
    public void setStam(int stamina) {this.currStamina = currStamina;}
    public void setMaxStam(int maxStamina) {this.maxStamina = maxStamina;}
    public void setPos(int[] pos) {this.pos = pos;}
    public void setXCoord(int x) {pos[0] = x;}
    public void setYCoord(int y) {pos[1] = y;}
    public void setCurrentChar(char c) {currentChar = c;}
    public void setDirection(char c) {plDir = c;}
    public void setShieldDura(int shieldDura) {this.shieldDura = shieldDura;}
}
